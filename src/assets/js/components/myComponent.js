export default class Component{
  constructor(name){
    this.name = name;
  }

  sayName(){
    return this.name;
  }

  render(){
    return `
      <div class="component">
        <h1>Привет, меня зовут ${this.sayName()}</h1>
      </div>
    `
  }
}